Pasos para la instalación de una clave SSH.

Primero podemos ir a nuestro cmd o terminal y poner el comando "ssh"

`ssh`

![Captura.PNG](../_resources/Captura-1.PNG)

Nos saldra algo asi. En caso contrario habría que instalarlo:

[Instalación en Windows](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)

Una vez que lo tenemos instalado procedemos a crear nuestra clave, pero antes debemos crear la carpeta ".ssh", del siguiente modo.

`mkdir .ssh`

![1.PNG](../_resources/1-1.PNG)

Siempre tenemos que estar ubicado con la terminal sobre nuestra carpeta de usuario, en mi caso mi usuario es "kobom".

Lo siguiente es ingresar y crear la clave:

`cd .ssh`
`ssh-keygen -t rsa`

![2.PNG](../_resources/2-1.PNG)

Nos va a preguntar si queremos poner un nombre a nuestra clave, si utilizar cuando se tiene cuentas diferentes (en caso que quiere configurarlo de esa manera me notifican), por ahora lo configuramos generalmente con un usuario.
Lo siguiente nos va a preguntar si queremos colocar una frase de seguridad, es una encriptación mucho más segura, pero no es necesario. 
Por lo que damos a la tecla "enter" tres veces.
![3.PNG](../_resources/3-1.PNG)

Nos saldrá lo siguiente:
![1ea7eeb0511c2174d97d68b14a55fa79.png](../_resources/1ea7eeb0511c2174d97d68b14a55fa79-1.png)

Ya generamos nuestra clave, ahora pasemos a incluirla en Gitlab
Ingresamos con nuestro usuario de gitlab a [GitLab](https://gitlab.com/)

En la parte superior a la derecha podemos ver las opciones de nuestro usuario e ingresar a "Preferencias"
![3502ec7496c223f75d3fc991572ac628.png](../_resources/3502ec7496c223f75d3fc991572ac628-1.png)
![1098569fa09a2564ae333e9b25748fb7.png](../_resources/1098569fa09a2564ae333e9b25748fb7-1.png)

Ahora volvemos a la terminal, e ingresamos el siguiente comando:

`type id_rsa.pub`

![2494e5e5637e798fdffb9289a5541672.png](../_resources/2494e5e5637e798fdffb9289a5541672-1.png)
Desde la terminal copiamos todo el texto que nos devolvió.
Lo registramos en gitlab
![38125a395e002dc2b176bf8af3608347.png](../_resources/38125a395e002dc2b176bf8af3608347-1.png)
Lo pegamos en el cuerpo y el título se pondrá solo, pero podemos cambiarlo, le damos añadir clave.

Este comando nos incluye la clave que generamos nueva.

`start-ssh-agent`

![2c4448454817fdd2e8bbccfeee7747cf.png](../_resources/2c4448454817fdd2e8bbccfeee7747cf-1.png)
Ahora vamos a comprobar que podamos conectarnos.

`ssh -T git@gitlab.com`

![6ff07083ab596e5e53e173003cd20b16.png](../_resources/6ff07083ab596e5e53e173003cd20b16-1.png)

En caso que les pregunte de esta manera:
![d898dd27054083835208f3201b750ee9.png](../_resources/d898dd27054083835208f3201b750ee9-1.png)
Le ponen yes, y fijense que ya me salta la conexión.

## !! Ya tienes configurada la clave ssh con tu cuenta gitlab.


