## Proyecto To_Do

***Resumen***

Este proyecto se centra en la práctica y teoría del framework VueJS.
***

* **Curva de Aprendizaje**:
1. A medida que van practicando y leyendo la documentación de lo más básico hasta lo más avanzado se refleja en este proyecto u otros.
2. Se dispondrá de un tiempo significativo para que podamos medir su curva, y también tener un objetivo a largo y corto plaza. Esto se explicará más adelante.
3. Recuerden que siempre está abierto el canal de slack para consultas, dudas o avisos en cuanto al seguimiento del proyecto. Tanto sea por el canal #general como el de #vuejs
4. Los días viernes a las 23hs se dispondrá del taller para ir implementando nuevas prácticas como también devoluciones antes errores o trabas en cuanto al proyecto.

***

* **Objetivo del Proyecto**

Correr una webapp en un servidor o localmente, como también todos los conceptos que conlleva el framework y la necesidad de la app.
***
* **Tecnologias a utilizar**

Las siguientes herramientas deberán tener instaladas para trabajar sobre el proyecto.

1. [Node JS version 10 o superior](https://nodejs.org/es/download/) 
2. [Git ultima version](https://git-scm.com/downloads)
3. [Visual Studio Code](https://code.visualstudio.com/download) u otro.
4. Explorador (Chrome, Firefox u otro)

(*) Vetur es una extensión de VSCODE que pueden instalar para visualizar el code de vuejs.
(**) Más adelante explicare la configuración de git.

* ****
* **Modalidad de Trabajo**

Siempre trabajaremos sobre el repositorio remoto, para poder verificar los avances, errores o mejoras en cuanto al código.

En este caso se usara como remoto la web [GitLab](https://gitlab.com/)
Si aún no han creado el usuario están invitados.
Desde mi parte ya tienen las invitaciones del grupo [Cilsa_FoxLab](https://gitlab.com/foxlab1)
Teniendo en cuenta el mail del perfil de slack de cada uno.

En otro archivo explico en detalle cómo utilizar claves ssh, para incorporar a su pc, y no tenes que ingresar usuario y contraseña para poder clonar o pushear cambios.

Configuración SSH.md

Ya configurado el ssh pasamos a clonar el repositorio.

`git clone git@gitlab.com:foxlab1/to_do.git`

(**NOTA**) Donde clone el repositorio será donde están ubicados con la terminal.

Ingresamos al proyecto.

`cd to_do`

Recuerden configurar el perfil de git para poder hacer los commits y push.

`git config user.name "NOMBRE"`

`git config user.mail EJEMPLO@gmail.com`

Cuando clonemos el repositorio remoto de gitlab, lo primero que tenemos que hacer es crear un branch con el nombre de cada uno, a continuación los comandos.

`git checkout -b  NAME`

(*) Siempre tenemos que estar ubicados en la rama main

**Seguimiento**

Gitlab nos permite hacer un hitos, que es nada menos que una ruta, para ir trazando nuestras tareas o incidencias (issues).

[Primer Hito](https://gitlab.com/foxlab1/to_do/-/milestones/1)
Tiene fecha hasta el 24 de diciembre, obviamente es una estimación. Puede pasar por encima de esa fecha, la idea general es medir su curva de aprendizaje. En el link pueden ingresar al hito para ver las premisas.

También se implementaran las [incidencias](https://gitlab.com/foxlab1/to_do/-/issues) donde se reflejaran las tareas, para medir a corto plazo. 
Donde también yo puedo abrir una incidencia por algo que falte o para mejorar.

Una vez que  hayan podido llegar hasta acá con todo la configuración pueden enviar un primer push para poder ir creando su primera incidencia con la tarea que deberán realizar.

* **

* **Entorno de Trabajo**

Para empezar a trabajar con el repositorio pueden realizar los siguientes comandos:

`npm install`

![64d4e8949c552f3ad10a5ff83e940cfa.png](../_resources/64d4e8949c552f3ad10a5ff83e940cfa-1.png)

Tengan en cuenta que para correr este comando tengo que estar ubicado en el proyecto.

Para iniciar el servidor:

`npm run serve`

![ec9b1404e4c25429318226e221cbcbc7.png](../_resources/ec9b1404e4c25429318226e221cbcbc7-1.png)

Una vez que termine el build de la app
![1af0d5f4c2dbb4c9fd660ebce6221cd6.png](../_resources/1af0d5f4c2dbb4c9fd660ebce6221cd6-1.png)

Ya pueden ingresar a la ruta:

['LocalHost'](http://localhost:8080/)

![5ac110d1247dc3136bd70a69c7abbf76.png](../_resources/5ac110d1247dc3136bd70a69c7abbf76-1.png)

## **Corriendo la App**

Los cambios que realizan se reflejarán.

