import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({

 /*    Aca pueden colocar la configuracion
    ya sea de la paleta de colores o configuraciones propias
    de Vuetify.
    dejo el link para dejar los colores.
    https://vuetifyjs.com/en/styles/colors/#javascript-color-pack */

});
